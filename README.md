# dcg-logrus-helper

## Getting Started

This package is intended to make working with a single instance of logrus easier, creating a LogDecorator struct
which can be injected into lambda handlers and DB structs.

On the cli run glide get;

```
glide get bitbucket.org/tastecard/dcg-logrus-helper
```

## Using the package


To Create a LogDecorator Struct you simply need to instanciate it
The Logger is the logrus instance, and the additional fields are the fields
that will be attached to all logs.

```
logrusInstance, _ := logrus.New()
logger := LogDecorator{
	Logger:           logrusInstance,
	AdditionalFields: logrus.Fields{
		"request_id": "12345",
    },
}
```

To log call the Log method, with the message and the level, NOTE info and error are only available atm.
```
logger.Log("Something non trivial", "Info")
```

The above will will output;

```
time="2018-10-03T13:43:48Z" level=info msg="Something non trivial" request_id=12345
```

### Addition one time additional log fields

You can add additional fields to only be used with one log, this is especially useful when you are logging metrics
and do not want the additional fields to carry over.

Initialise as you normally would
````
inputlogger, hook := test.NewNullLogger()
inputlogger.Level = logrus.DebugLevel
logger := LogDecorator{
	Logger: inputlogger,
	AdditionalFields: logrus.Fields{},
}
````

Then call

````
logger.SetTempAdditionalFields(logrus.Fields{"code": "one",}).Log("msg", "info")
````
The additional fields will be code = one

and then logger is called again

````
logger.Log("something else", "info")
````
The code = one will not be present
