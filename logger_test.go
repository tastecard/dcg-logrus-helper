package logger

import (
	"fmt"
	"reflect"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/sirupsen/logrus/hooks/test"
)

func TestLogMethodLogsAsExpected(t *testing.T) {
	t.Parallel()

	// create logrus
	inputlogger, hook := test.NewNullLogger()
	// create logarus field
	additionalFields := logrus.Fields{
		"request_id": "12345",
	}
	// create log decorator
	logger := LogDecorator{
		Logger:           inputlogger,
		AdditionalFields: additionalFields,
	}

	inputLogMessage := "Some Error"

	logger.Log(inputLogMessage, "Info")

	outputLogMessage := hook.Entries[0].Message

	if inputLogMessage != outputLogMessage {
		t.Errorf("incorrect log message returned, expected: %s, output: %s", inputLogMessage, outputLogMessage)
	}
}

func TestCanAddFieldToAllLogEntries(t *testing.T) {
	t.Parallel()

	// create logarus
	inputlogger, hook := test.NewNullLogger()
	// create logarus field
	additionalFields := logrus.Fields{
		"request_id": "12345",
	}
	// create log decorator
	logger := LogDecorator{
		Logger:           inputlogger,
		AdditionalFields: additionalFields,
	}

	logger.Log("Arrgh", "info")

	// Should have additional fields added
	for i, field := range additionalFields {
		if field != hook.Entries[0].Data[i] {
			t.Errorf("additional fields do not match those injected, expecting %+v\n, output: %+v\n", additionalFields, hook.Entries[0].Data)
		}
	}
}

func TestCorrectLevelsLogged(t *testing.T) {
	t.Parallel()

	testCases := []struct {
		name string
		// Input / Expected
		level         string
		expectedLevel string
	}{
		{
			"Info level is correct",
			"info",
			"info",
		},
		{
			"Error level is correct",
			"error",
			"error",
		},
		{
			"Uppercase Error level is correct",
			"Error",
			"error",
		},
		{
			"Unknown level is correct",
			"unknown",
			"error",
		},
		{
			"Debug level is correct",
			"debug",
			"debug",
		},
		{
			"Warn level is correct",
			"Warn",
			"warning",
		},
		// Unable to test panic() and fatal as this would stop the script :(
	}

	for _, tc := range testCases {

		inputlogger, hook := test.NewNullLogger()
		inputlogger.Level = logrus.DebugLevel
		logger := LogDecorator{
			Logger: inputlogger,
			AdditionalFields: logrus.Fields{
				"request_id": "12345",
			},
		}

		logger.Log(fmt.Sprintf("I have logged a %s log", tc.level), tc.level)

		fmt.Printf("%+v\n", hook.Entries)

		levelOutput := hook.Entries[0].Level.String()

		if levelOutput != tc.expectedLevel {
			t.Errorf("%s, incorrect level returned, expected %s, output: %s", tc.name, tc.expectedLevel, levelOutput)
		}
	}
}

//
func TestSetTempAdditionalFieldsReturnsAsExpected(t *testing.T) {

	// Arrange
	expectedFirstAndThirdLog := logrus.Fields{
		"request_id": "12345",
	}

	expectedSecondLogFields := logrus.Fields{
		"request_id":       "12345",
		"additional_field": "aaa",
	}

	inputlogger, hook := test.NewNullLogger()
	inputlogger.Level = logrus.DebugLevel
	logger := LogDecorator{
		Logger:           inputlogger,
		AdditionalFields: expectedFirstAndThirdLog,
	}

	// Act
	logger.Log("AAAA", "error")

	logger.SetTempAdditionalFields(logrus.Fields{
		"additional_field": "aaa",
	}).Log("BBBB", "error")

	logger.Log("CCCC", "error")

	// Assert that temp additional fields were added to the first log
	if !reflect.DeepEqual(expectedFirstAndThirdLog, hook.Entries[0].Data) {
		t.Errorf("log one fields not as expected,\nexpected:%+v\noutcome:%+v", expectedFirstAndThirdLog, hook.Entries[0].Data)
	}

	if !reflect.DeepEqual(expectedSecondLogFields, hook.Entries[1].Data) {
		t.Errorf("log two fields not as expected,\nexpected:%+v\noutcome:%+v", expectedSecondLogFields, hook.Entries[1].Data)
	}

	if !reflect.DeepEqual(expectedFirstAndThirdLog, hook.Entries[2].Data) {
		t.Errorf("log two fields not as expected,\nexpected:%+v\noutcome:%+v", expectedFirstAndThirdLog, hook.Entries[2].Data)
	}
}
