package logger

import (
	"github.com/sirupsen/logrus"
	"strings"
)

// LogDecorator is a helpful wrapper for logrus, saving you from constantly adding additional fields to each log
// set once and there will appear on every log, depending you are using the LogDecorator.Log("msg", "info")
type LogDecorator struct {
	Logger           *logrus.Logger
	AdditionalFields logrus.Fields
}

// Log logs and attaches the additional fields that were set earlier
func (ld LogDecorator) Log(msg, level string) {

	switch strings.ToLower(level) {
	case "info":
		ld.Logger.WithFields(ld.AdditionalFields).Info(msg)
	case "error":
		ld.Logger.WithFields(ld.AdditionalFields).Error(msg)
	case "debug":
		ld.Logger.WithFields(ld.AdditionalFields).Debug(msg)
	case "warn":
		ld.Logger.WithFields(ld.AdditionalFields).Warn(msg)
	case "fatal":
		ld.Logger.WithFields(ld.AdditionalFields).Fatal(msg)
	case "panic":
		ld.Logger.WithFields(ld.AdditionalFields).Panic(msg)
	default:
		ld.Logger.WithFields(ld.AdditionalFields).Error(msg)
	}
}

// SetTempAdditionalFields allows you to add additional fields for one log only, in a chaining type of manor
// eg LogDecorator.SetTempAdditionalFields(..).Log("", "") ... is only logged for one request
func (ld LogDecorator) SetTempAdditionalFields(fields logrus.Fields) LogDecorator {

	newFields := logrus.Fields{}
	for k, v := range ld.AdditionalFields {
		newFields[k] = v
	}
	for k, v := range fields {
		newFields[k] = v
	}

	return LogDecorator{
		Logger:           ld.Logger,
		AdditionalFields: newFields,
	}
}
